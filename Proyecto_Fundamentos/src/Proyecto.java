

import java.util.Date;
import java.text.ParseException;

import java.util.Arrays;


public class Proyecto {
    public static void main(String[] args) throws ParseException {
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy");
        // Creamos el arreglo de fechas
        // Vamos a convertir un arreglo de texto a fecha.

        String FechaTexto[] = {"28-06-2021", "29-06-2021", "14-06-2021"};
        Date[] FechaCalendario = new Date[FechaTexto.length];           // creamos un arreglo vacio de Fechas

        // Hacemos la conversion de las fechas que inicialmente se crearon como tipo  String a tipo Calendario
        for (int i = 0; i < FechaTexto.length; i++) {
            FechaCalendario[i] = sdf.parse(FechaTexto[i]);
        }

        // Creamos los arreglos de Ingresos y Gastos (Egresos) de caja.

        double Ingresos[] = {20.0, 10.0, 50.0};
        double Egresos[] = {30.0, 10.0, 50.0};

        // Quiero ordenar mi arreglo de fecha de forma ascendente.
        // Quiero que mis Ingresos y Egresos se muestren en el orden de las fechas.
        // Finalmente, quiero un arreglo que me indique si los Ingresos y Egresos son iguales
        ;
        System.out.println("Arreglo de fechas inicial: " + Arrays.toString(FechaCalendario));
        System.out.println("Arreglo de fechas ordenado: " + Arrays.toString(OrdenarFechas(FechaCalendario)));
        System.out.println("Arreglo de Ingresos inicial: " + Arrays.toString(Ingresos));
        System.out.println("Ingresos Ordenado: " + Arrays.toString(ArregloIngresos(FechaCalendario, Ingresos)));
        System.out.println("Arreglo de Egresos inicial: " + Arrays.toString(Egresos));
        System.out.println("Egresos Ordenado: " + Arrays.toString(ArregloEgreso(FechaCalendario, Egresos)));
        System.out.println("Mensaje de resultados del programa: " + Arrays.toString(ArregloComparacion(FechaCalendario, Ingresos, Egresos)));
    }
    // Creo el primer modulo para ordenar las fechas de forma ascendente (de menor a mayor)
    public static Date[] OrdenarFechas(Date[] arreglo) {
        Date respuesta[] = new Date[arreglo.length];
        int j;
        Date valor;
        int contador = 0;

        for (int i = 1; i < arreglo.length; i++) {
            j = i;
            valor = arreglo[i];

            while (j > 0 && valor.before(arreglo[j - 1])) {
                arreglo[j] = arreglo[j - 1];
                j = j - 1;
            }
            arreglo[j] = valor;
        }
        return arreglo;
    }
    // Creo un segundo modulo para ordenar los Ingresos de acuerdo al orden de las Fechas.
    public static double[] ArregloIngresos(Date[] arreglo, double[] Ingresos1) {
        //Date respuesta1[] = new Date[arreglo.length];
        double IngresosOrdenado[] = new double[Ingresos1.length];
        int j;
        double valor2;
        Date valor;
        int contador = 0;

        for (int i = 1; i < arreglo.length; i++) {
            j = i;
            valor = arreglo[i];
            valor2 = Ingresos1[i];

            while (j > 0 && valor.before(arreglo[j - 1])) {
                arreglo[j] = arreglo[j - 1];
                Ingresos1[j] = Ingresos1[j - 1];
                j = j - 1;
            }
            arreglo[j] = valor;
            IngresosOrdenado[j] = Ingresos1[j - 1];
        }
        return IngresosOrdenado;
    }
    // Creo un segundo modulo para ordenar los Egresos de acuerdo al orden de las Fechas.
    public static double[] ArregloEgreso(Date[] arreglo, double[] Egresos1) {
        //Date respuesta1[] = new Date[arreglo.length];
        double EgresosOrdenado[] = new double[Egresos1.length];
        int j;
        double valor2;
        Date valor;
        int contador = 0;

        for (int i = 1; i < arreglo.length; i++) {
            j = i;
            valor = arreglo[i];
            valor2 = Egresos1[i];

            while (j > 0 && valor.before(arreglo[j - 1])) {
                arreglo[j] = arreglo[j - 1];
                Egresos1[j] = Egresos1[j - 1];
                j = j - 1;
            }
            arreglo[j] = valor;
            EgresosOrdenado[j] = Egresos1[j - 1];
        }
        return EgresosOrdenado;
    }
    // Creo un cuarto modulo para tener un arreglo en String que me indique si los montos de Ingresos y Egresos entre
    // empresas relacionadas son iguales.
    // Utilizo recursividad ya que hago un llamado a 2 modulos creados previamente: "ArregloIngresos" y "ArregloEgresos"

    public static String[] ArregloComparacion(Date[] arreglo, double[] Ingresos, double[] Egresos) {
        String Comparacion[] = new String[Egresos.length];
        double[] IngresosOrdenado = ArregloIngresos(arreglo, Ingresos);
        double[] EgresosOrdenado = ArregloEgreso(arreglo, Egresos);
        int contador = 0;
        for (int i = 0; i < Egresos.length; i++) {
            if (IngresosOrdenado[i] == EgresosOrdenado[i]) {
                Comparacion[contador] = "Montos iguales";
            } else {
                Comparacion[contador] = "Montos diferentes";
            }
            contador++;
        }
        return Comparacion;
    }
}








